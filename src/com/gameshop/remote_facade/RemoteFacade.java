package com.gameshop.remote_facade;

import com.gameshop.dtos.OrderDTO;
import com.gameshop.dtos.ProductDTO;
import com.gameshop.mappers.AddressMapper;
import com.gameshop.mappers.OrderMapper;
import com.gameshop.mappers.ProductMapper;
import com.gameshop.models.Order;
import com.gameshop.models.users.Address;

/** A global facade which implements remote facade pattern */
public class RemoteFacade {
	public ProductDTO getProductDTO(int id) {
		return ProductDTO.writeDTO(ProductMapper.find(id).get());
	}
	
	public String getProductXml(int id) {
		ProductDTO dto = new ProductDTO(ProductMapper.find(id).get());
		return ProductDTO.toXML(dto);
	}
	
	public OrderDTO getOrderDTO(int id) {
		Order order;
		order = OrderMapper.find(id).get();
		Address addr = order.getDeliverAddress();
		
		return new OrderDTO(order);
	}
	
	public OrderDTO getOrderXml(int id) {
		OrderDTO dto = getOrderDTO(id);
		return OrderDTO.toXML(dto);
	}
}
