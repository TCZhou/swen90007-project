package com.gameshop.utilities.interfaces;

public interface LockManager {
	public boolean acquireLock(long lockable, String owner);
	public boolean releaseLock(long lockable, String owner);
}
