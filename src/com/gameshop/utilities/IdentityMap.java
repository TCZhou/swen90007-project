package com.gameshop.utilities;

import java.util.Map;
import java.util.HashMap;

@SuppressWarnings("rawtypes")
public class IdentityMap<E> {

	private Map<Integer, E> map = new HashMap<Integer, E>();

	private static Map<Class, IdentityMap> singletons = new HashMap<Class, IdentityMap>();

	@SuppressWarnings("unchecked")
	public static <E> IdentityMap<E> getInstance(E e) {
		IdentityMap<E> result = singletons.get(e.getClass());

		if (result == null) {
			result = new IdentityMap<E>();
			singletons.put(e.getClass(), result);
		}

		return result;
	}

	public void save(int id, E obj) {
		map.put(id, obj);
	}

	public E find(int id) {
		return map.getOrDefault(id, null);
	}
}
