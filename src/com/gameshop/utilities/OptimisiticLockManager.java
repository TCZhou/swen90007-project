package com.gameshop.utilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.gameshop.db.DBConnection;
import com.gameshop.utilities.interfaces.LockManager;

public class OptimisiticLockManager<E> {
	// Single instance
	private static Connection conn = DBConnection.getConnection();
	private static final String VERSION_COLUMN = "ver";
	private static Map<Class, OptimisiticLockManager> singletons = new HashMap<Class, OptimisiticLockManager>();
//	private Map<Integer, E> versionMap = new HashMap<Integer, E>();
	private String tableName;
	
	public static <E> OptimisiticLockManager<?> getInstance(E e, String tableName) {
		OptimisiticLockManager<?> result = singletons.get(e.getClass());

		if (result == null) {
			result = new OptimisiticLockManager<E>(tableName);
			singletons.put(e.getClass(), result);
		}

		return result;
	}
	
	// Make constructor private to prevent initializations outside of the class
	private OptimisiticLockManager(String tableName) {
//		versionMap = new HashMap<> ();
		this.tableName = tableName;
	}

	private static String FIND_VERSION = "SELECT " + VERSION_COLUMN + " FROM  public.? WHERE id=?";
	public int acquireVersion(int lockable) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int result = -1;
		// if the owner already has the lock, this should succeed 
		// straightforward query to the lock table to see if the pair is already in the table
		// assume there is a version column named "ver", and the primary key column is named "id"
		try {
			ps = conn.prepareStatement(FIND_VERSION);
			ps.setString(1, this.tableName);
			ps.setInt(2, lockable);
			
			rs = ps.executeQuery();
			
			result = rs.getInt(VERSION_COLUMN);
		} catch (SQLException e) { 
			 result = -1;
		} finally {
			try {
				ps.close();
				rs.close();
			} catch (SQLException | NullPointerException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	private static String UPDATE_VERSION = "UPDATE SET " + VERSION_COLUMN + "=? FROM  public.? WHERE id=? AND " + VERSION_COLUMN + "=?";
	public boolean updateVersion(int id, int currentVersion) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean result = false;
		
		try {
			ps = conn.prepareStatement(UPDATE_VERSION);
			ps.setInt(1, currentVersion + 1);
			ps.setString(2, this.tableName);
			ps.setInt(3, id);
			ps.setInt(4, currentVersion);

			int rowAffected = ps.executeUpdate();
			
			if (rowAffected == 1) {
				result = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Error when updating " + this.tableName + " table with id = " + id + " and version = " + currentVersion);
		}
		
		return result;
	}
}
