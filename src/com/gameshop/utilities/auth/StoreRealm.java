package com.gameshop.utilities.auth;

import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;

import com.gameshop.models.users.Administrator;
import com.gameshop.models.users.Customer;
import com.gameshop.models.users.User;
import com.gameshop.services.UserService;

public class StoreRealm extends JdbcRealm {
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		UsernamePasswordToken userPassToken = (UsernamePasswordToken) token;
		final String username = userPassToken.getUsername();
		
		final User user = UserService.getUserWithAuthInfo(username);
		if (user == null) {
			System.out.print("No account found for user with username " + username);
			return null;
		}
		
		return new SimpleAuthenticationInfo(user.getUserId(), user.getAuthenticationInfo().getPassword(), getName());
	}
	
	@Override
	protected AuthorizationInfo getAuthorizationInfo(PrincipalCollection principals) {
		Set<String> roles = new HashSet<>();
		
		if (principals.isEmpty()) {
			return null;
		}
		
		int userid = (Integer) principals.getPrimaryPrincipal();
		final User user = UserService.getUser(userid);
		
		if (user instanceof Administrator) {
			roles.add("administrator");
		} else if (user instanceof Customer) {
			roles.add("customer");
		}
				
		return new SimpleAuthorizationInfo(roles);
	}
}
