package com.gameshop.dtos;

import com.gameshop.models.cart.ShoppingCartItem;

public class ShoppingCartItemDTO {
	private ProductDTO product;
	private int quantity;
	
	public ShoppingCartItemDTO(ProductDTO product, int quantity) {
		this.product = product;
		this.quantity = quantity;
	}
	
	public ShoppingCartItemDTO(ShoppingCartItem item) {
		this.product = new ProductDTO(item.getProduct());
		this.quantity = item.getQuantity();
	}
	
	public ProductDTO getProduct() {
		return product;
	}

	public void setProduct(ProductDTO product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
