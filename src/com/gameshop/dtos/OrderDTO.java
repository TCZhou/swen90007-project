package com.gameshop.dtos;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.gameshop.models.Order;
import com.gameshop.models.Product;
import com.gameshop.models.cart.ShoppingCartItem;
import com.gameshop.models.users.Address;
import com.gameshop.models.users.PaymentMethod;

public class OrderDTO {
	private int orderId;
	private int customerId;
	private String orderDate;
	private double total;
	private PaymentMethod paymentMethod;
	private Address deliverAddress;
	private List<ShoppingCartItemDTO> items;
	
	public OrderDTO() {
	}
	
	public OrderDTO(Order order) {
		super();
		this.orderId = order.getOrderId();
		this.customerId = order.getCustomerId();
		this.orderDate = order.getOrderDate();
		this.total = order.getTotal();
		this.paymentMethod = order.getPaymentMethod();
		this.deliverAddress = order.getDeliverAddress();
		makeItems(order.getItems());
	}

	
	private void makeItems(List<ShoppingCartItem> shoppingCartItems) {
		List<ShoppingCartItemDTO> outputList = new ArrayList<ShoppingCartItemDTO>();
		for (ShoppingCartItem item : shoppingCartItems) {
			outputList.add(new ShoppingCartItemDTO(item));
		}
		this.items = outputList;
	}


	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public Address getDeliverAddress() {
		return deliverAddress;
	}
	public void setDeliverAddress(Address deliverAddress) {
		this.deliverAddress = deliverAddress;
	}

	public List<ShoppingCartItemDTO> getItems() {
		return items;
	}

	public static OrderDTO toXML(OrderDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static String toXML(ProductDTO productDTO) {
    	ByteArrayOutputStream stream = new ByteArrayOutputStream();
        XMLEncoder encoder = new XMLEncoder(stream);
        encoder.writeObject(productDTO);
        encoder.close();
		return stream.toString();
    }

    public static ProductDTO fromXML(String str) {
    	ByteArrayInputStream stream = new ByteArrayInputStream(str.getBytes());
        XMLDecoder decoder = new XMLDecoder(stream);
        ProductDTO result = (ProductDTO) decoder.readObject();
        decoder.close();
        return result;
    }

	public static ProductDTO writeDTO(Product product) {
		return new ProductDTO(product);
	}
}
