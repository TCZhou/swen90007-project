package com.gameshop.dtos;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import com.gameshop.mappers.ProductMapper;
import com.gameshop.models.Product;
import com.gameshop.utilities.OptimisiticLockManager;

/**
 * Product model
 */
public class ProductDTO implements Serializable {
	/**
	 * Serial Version ID
	 */
	private static final long serialVersionUID = 1L;

	private int productId;
	private String title;
	private double price;
	private String description;
	private String category;
	private String publisher;
	private int inventory;

	public ProductDTO() {
	}

	public ProductDTO(int i, String productName, double productPrice,
			String description, String category, String productManufacturer) {
		super();
		this.productId = i;
		this.title = productName;
		this.price = productPrice;
		this.description = description;
		this.category = category;
		this.publisher = productManufacturer;
	}
	
	public ProductDTO(int id, String title, String publisher, double price, int inventory) {
		super();
		this.productId = id;
		this.title = title;
		this.price = price;
		this.publisher = publisher;
		this.setInventory(inventory);
	}
	
	public ProductDTO(int id, String title, String publisher, double price) {
		super();
		this.productId = id;
		this.title = title;
		this.price = price;
		this.publisher = publisher;
	}
	
	public ProductDTO(Product product) {
		super();
		this.productId 	= product.getProductId();
		this.title 		= product.getProductName();
		this.price 		= product.getProductPrice();
		this.publisher 	= product.getPublisher();
		this.description = product.getDescription();
		this.category 	= product.getDescription();
		this.setInventory(product.getInventory());
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return title;
	}

	public void setProductName(String productName) {
		this.title = productName;
	}

	public double getProductPrice() {
		return price;
	}

	public void setProductPrice(double productPrice) {
		this.price = productPrice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	
    public int getInventory() {
		return inventory;
	}

	public void setInventory(int inventory) {
		this.inventory = inventory;
	}

	public static String toXML(ProductDTO productDTO) {
    	ByteArrayOutputStream stream = new ByteArrayOutputStream();
        XMLEncoder encoder = new XMLEncoder(stream);
        encoder.writeObject(productDTO);
        encoder.close();
		return stream.toString();
    }

    public static ProductDTO fromXML(String str) {
    	ByteArrayInputStream stream = new ByteArrayInputStream(str.getBytes());
        XMLDecoder decoder = new XMLDecoder(stream);
        ProductDTO result = (ProductDTO) decoder.readObject();
        decoder.close();
        return result;
    }

	public static ProductDTO writeDTO(Product product) {
		return new ProductDTO(product);
	}
	
	public static Product loadDTO(ProductDTO dto) {
		Product p = new Product();
		
		// Initialize
		p.setProductId(dto.getProductId());
		p.setProductName(dto.getProductName());
		p.setProductPrice(dto.getProductPrice());
		p.setDescription(dto.getDescription());
		p.setCategory(dto.getCategory());
		p.setPublisher(dto.getPublisher());
		p.setInventory(dto.getInventory());
		p.setVersion(ProductMapper.findVersion(p.getProductId()));
		
		return p;
	}
}
