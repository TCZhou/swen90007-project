package com.gameshop.mappers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.gameshop.db.DBConnection;
import com.gameshop.models.Order;
import com.gameshop.utilities.IdentityMap;

public class OrderMapper {
	private static Connection conn = DBConnection.getConnection();
	private static IdentityMap<Order> orderIdentityMap = createIdentityMap();

	private static IdentityMap<Order> createIdentityMap() {
		Order temp= new Order();
		return IdentityMap.getInstance(temp);
	}

	private static final int INVALID_ID = -1;
	
	public static List<Order> findAllOrders() {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql;
		
		sql = "select * from public.order";

		List<Order> products = new ArrayList<Order>();

		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				Order p = null;
				p = orderIdentityMap.find(rs.getInt("id"));
				if (p == null) {
					//int orderId, int customerId, String orderDate, double total, PaymentMethod paymentMethod, Address deliverAddress
					p = new Order(rs.getInt("id"),
							rs.getInt("customer_id"),
							rs.getString("order_date"),
							rs.getDouble("total"),
							rs.getInt("payment_method"),
							rs.getInt("address"));
					orderIdentityMap.save(rs.getInt("id"), p);
				} else {
					updateOrderValue(p, rs);
				}
				products.add(p);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private static void updateOrderValue(Order p, ResultSet rs) throws SQLException {
		if (p == null) {
			p = new Order();
		}
		
		p.setOrderId(rs.getInt("id"));
		p.setCustomerId(rs.getInt("customer_id"));
		p.setOrderDate(rs.getString("order_date"));
		p.setTotal(rs.getDouble("total"));
		p.setPaymentMethodId(rs.getInt("payment_method"));
		p.setAddressId(rs.getInt("address"));
	}

	public static Optional<Order> find(int orderId) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql;

		Order p = null;
		
		// Search the product in identify map first
		p = orderIdentityMap.find(orderId);
		if (p != null) {
			return Optional.of(p);
		} else {
			p = new Order();
		}
		
		sql = "select * from public.order where id=?";

		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, orderId);
			rs = ps.executeQuery();
			while (rs.next()) {
				updateOrderValue(p, rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (p == null) {
			return Optional.empty();
		} else {
			IdentityMap.getInstance(p).save(orderId, p);
			return Optional.of(p);	
		}
	}
	
	public static int insert(Order orderToBeInserted) {
		String sql = "INSERT INTO public.order (customer_id, order_date, total, payment_method, address) VALUES (?,?,?,?,?)";
		PreparedStatement ps = null;
				
		int id = INVALID_ID;
		
		try {
			ps = conn.prepareStatement(sql);
 
			ps.setInt(1, orderToBeInserted.getCustomerId());
            ps.setString(2, orderToBeInserted.getOrderDate());
            ps.setDouble(3, (double) orderToBeInserted.getTotal());
            ps.setInt(4, orderToBeInserted.getPaymentMethod().getPaymentMethodId());
            ps.setInt(5, orderToBeInserted.getDeliverAddress().getId());
 
            int affectedRows = ps.executeUpdate();

            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getInt(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
		commit();
        return id;
	}
		
	public static boolean update(Order orderToBeUpdated) {
		
		String sql = "UPDATE public.product SET product_name=?,publisher=?,price=? WHERE id = ?";
		
		PreparedStatement ps = null;
		
		int rowAffected = 0;
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, orderToBeUpdated.getCustomerId());
            ps.setString(2, orderToBeUpdated.getOrderDate());
            ps.setDouble(3, (double) orderToBeUpdated.getTotal());
            ps.setInt(4, orderToBeUpdated.getPaymentMethod().getPaymentMethodId());
            ps.setInt(5, orderToBeUpdated.getDeliverAddress().getId());
            ps.setInt(6, orderToBeUpdated.getOrderId());
            
			rowAffected = ps.executeUpdate();
			System.out.println(rowAffected + " row(s) updated.");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		commit();
		return rowAffected > 0;
	}
	
	private static void commit() {
		// Commit the changes to database
		try {
			conn.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
