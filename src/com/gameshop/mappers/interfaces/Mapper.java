package com.gameshop.mappers.interfaces;

import com.gameshop.utilities.IdentityMap;

public interface Mapper<T> {
	public T find (int id); 
	public void insert (T obj);
	public void update (T obj);
	public void delete (T obj);
}