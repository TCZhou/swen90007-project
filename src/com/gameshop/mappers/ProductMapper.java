package com.gameshop.mappers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.gameshop.db.DBConnection;
import com.gameshop.models.Product;
import com.gameshop.utilities.IdentityMap;
import com.gameshop.utilities.OptimisiticLockManager;

/** 
 * Implicit lock is implemented in this class, 
 * the lock will be acquired in the find functions 
 * and will be used for update and delete methods 
 */
public class ProductMapper {
	private static Connection conn = DBConnection.getConnection();
	private static final Product tempProduct = new Product();
	private static IdentityMap<Product> productIdentityMap = IdentityMap.getInstance(tempProduct);

	private static final int INVALID_ID = -1;
	
	private static void updateProductValue(Product product, ResultSet rs) throws SQLException {
		product.setProductName(rs.getString("product_name"));
		product.setPublisher(rs.getString("publisher"));
		product.setProductPrice(rs.getDouble("price"));	
		product.setDescription(rs.getString("description"));
		product.setCategory(rs.getString("category"));
	}
	
	public static List<Product> findAllProducts() {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql;
		
		sql = "select * from public.product";

		List<Product> products = new ArrayList<Product>();

		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				Product p = null;
				p = productIdentityMap.find(rs.getInt("id"));
				if (p == null) {
					p = new Product(rs.getInt("id"),
							rs.getString("product_name"),
							rs.getString("publisher"),
							rs.getDouble("price"));
					p.setDescription(rs.getString("description"));
					p.setCategory(rs.getString("category"));
					p.setInventory(rs.getInt("inventory"));
					// Get the version of the product, for the implicit optimistic lock
					p.setVersion(rs.getInt("ver"));
					productIdentityMap.save(rs.getInt("id"), p);
				} else {
					updateProductValue(p, rs);
				}
				products.add(p);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	public static Optional<Product> find(int productId) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql;

		Product p;
		
		// Search the product in identify map first
		p = productIdentityMap.find(productId);
		if (p != null) {
			return Optional.of(p);
		}
		
		p = new Product();
		p.setProductId(INVALID_ID);
		
		sql = "select * from public.product where id=?";

		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, productId);
			rs = ps.executeQuery();
			while (rs.next()) {
				p.setProductId(productId);
				p.setProductName(rs.getString("product_name"));
				p.setProductPrice(rs.getDouble("price"));	
				p.setDescription(rs.getString("description"));
				p.setCategory(rs.getString("category"));
				p.setPublisher(rs.getString("publisher"));
				p.setInventory(rs.getInt("inventory"));
				// Get the version of the product, for the implicit optimistic lock
				p.setVersion(rs.getInt("ver"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (p.getProductId() == INVALID_ID) {
			return Optional.empty();
		} else {
			IdentityMap.getInstance(p).save(productId, p);
			return Optional.of(p);	
		}
	}
	
	public static int findVersion(int id) {
		Product temp = new Product();
		OptimisiticLockManager<Product> lm = (OptimisiticLockManager<Product>) OptimisiticLockManager.getInstance(temp, "product");
		return lm.acquireVersion(id);
	}
	
	public static int insert(Product productToBeInserted) {
		String sql = "INSERT INTO public.product (product_name, publisher, price) VALUES (?,?,?)";
		PreparedStatement ps = null;
		
		int id = INVALID_ID;
		
		try {
			ps = conn.prepareStatement(sql);
 
            ps.setString(1, productToBeInserted.getProductName());
            ps.setString(2, productToBeInserted.getPublisher());
            ps.setFloat(3, (float) productToBeInserted.getProductPrice());
 
            int affectedRows = ps.executeUpdate();

            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getInt(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
		commit();
        return id;
	}
	
	public static int delete(Product productToBeDeleted) {
		PreparedStatement ps = null;
		int rowsAffected = 0;
		
		String sql = "delete from public.product where id=? AND ver=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, productToBeDeleted.getProductId());
			ps.setInt(2, productToBeDeleted.getVersion());
			rowsAffected = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println(rowsAffected + " row(s) deleted.");
		commit();
		return rowsAffected;
	}
	
	public static boolean update(Product productToBeUpdated) {		
		String sql = "UPDATE public.product SET product_name=?,publisher=?,price=?,ver=? WHERE id = ? AND ver=?";
		
		PreparedStatement ps = null;
		
		int rowAffected = 0;
		
		try {
			conn.setAutoCommit(false);
			ps = conn.prepareStatement(sql);
			ps.setString(1, productToBeUpdated.getProductName());
			ps.setString(2, productToBeUpdated.getPublisher());
			ps.setDouble(3, productToBeUpdated.getProductPrice());
			ps.setInt(4, productToBeUpdated.getVersion() + 1);
			ps.setInt(5, productToBeUpdated.getProductId());
			ps.setInt(6, productToBeUpdated.getVersion());
			
			rowAffected = ps.executeUpdate();
			System.out.println(rowAffected + " row(s) updated.");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		commit();
		
		try {
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rowAffected > 0;
	}
	
	private static void commit() {
		// Commit the changes to database
		try {
			conn.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
