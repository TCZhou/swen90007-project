package com.gameshop.mappers;

import com.gameshop.mappers.interfaces.Mapper;
import com.gameshop.utilities.IdentityMap;
import com.gameshop.utilities.interfaces.LockManager;

public abstract class LockingMapper<T> implements Mapper<T> {
	
	private LockManager lm;
	
	protected static <T> IdentityMap<T> getIdentityMap() {
		return null;
	}
	
	@Override
	public abstract T find(int id);

	@Override
	public abstract void insert(T obj);

	@Override
	public abstract void update(T obj);
	
	@Override
	public abstract void delete(T obj);
}
