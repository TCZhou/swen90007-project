package com.gameshop.mappers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.gameshop.db.DBConnection;
import com.gameshop.models.users.Address;
import com.gameshop.models.users.Administrator;
import com.gameshop.models.users.AuthenticationInfo;
import com.gameshop.models.users.Customer;
import com.gameshop.models.users.PaymentMethod;
import com.gameshop.models.users.User;
import com.gameshop.utilities.IdentityMap;

public class UserMapper {
	private static Connection conn = DBConnection.getConnection();
	private static final int INVALID_ID = -1;
	
	final private static String findUserByUsername = "SELECT * FROM public.user WHERE name=?";
	public static Optional<User> findByUsername(String username) {
		// the user id is unknown, not applicable to user identity map in this case
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = null;
		
		try {
			ps = conn.prepareStatement(findUserByUsername);
			ps.setString(1, username);
			rs = ps.executeQuery();
			while (rs.next()) {
				user = new Customer(rs.getInt("id"), 
						rs.getString("name"), 
						rs.getString("email"),
						rs.getString("address"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return Optional.ofNullable(user);
	}
	
	public static Optional<User> find(int userId) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql;

		User user = new Customer(INVALID_ID, null);
		
		// First check the existence of the user from the identity map
		User tempCustomer = IdentityMap.getInstance(user).find(userId);
		if (tempCustomer != null) {
			return Optional.of(tempCustomer);
		}
		
		sql = "SELECT * FROM public.user WHERE id=?";
		
		String sqlCheckAdmin = "SELECT COUNT(*) > 0 AS doExist FROM public.administrator WHERE user_id=?";
		// Get information from user table
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				user.setUserId(userId);
				user.setUsername(rs.getString("name"));
				user.setEmail(rs.getString("email"));
				user.setAddress(rs.getString("address"));
			}
			ps.close();
			rs.close();
			// Check if the user is admin
			ps = conn.prepareStatement(sqlCheckAdmin);
			ps.setInt(1, userId);
			rs = ps.executeQuery();
			while(rs.next()) {
				boolean idDoExists = rs.getBoolean("doExist");
				if (idDoExists) {
					String username = user.getUsername();
					user = new Administrator(userId, username);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		// Check if the user is an administrator
		try {
			ps = conn.prepareStatement(sqlCheckAdmin);
			ps.setInt(1, userId);
			rs = ps.executeQuery();
			if (!rs.wasNull()) {
				user = new Administrator(user.getUserId(), user.getUsername());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (user.getUserId() == INVALID_ID) {
			return Optional.empty();
		} else {
			IdentityMap.getInstance(user).save(userId, user);
			return Optional.of(user);
		}
	}
	
	public static int insert(User userToBeInserted) {
		String sql = "INSERT INTO public.user (username) VALUES (?)";
		PreparedStatement ps = null;
		
		int id = INVALID_ID;
		
		try {
			ps = conn.prepareStatement(sql);
 
            ps.setString(1, userToBeInserted.getUsername());
 
            int affectedRows = ps.executeUpdate();

            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getInt(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return id;
	}
	
	/**
	 * Insert and return with the generated new id
	 */
	public static int insert(Customer userToBeInserted) {
		
		String sql = "INSERT INTO public.user (username) VALUES (?)";
		String sql2 = "INSERT INTO public.customers (id) values(?)";
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		
		int id = INVALID_ID;
		
		try {
			ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, userToBeInserted.getUsername());
 
            int affectedRows = ps.executeUpdate();

            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getInt(1);
                    }
                    
                    // Run the second query
                    ps2 = conn.prepareStatement(sql2);
                    ps2.setInt(1, id);
                    int affectedRows2 = ps2.executeUpdate();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }        
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
        	try {
    			if (ps != null) {
    				ps.close();
    			}
    			if (ps2 != null) {
    				ps2.close();
    			}
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
        }
        return id;
	}
	
	public static int delete(User productToBeDeleted) {
		PreparedStatement ps = null;
		int rowsAffected = 0;
		
		String sql = "delete from public.user where id=?";
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, productToBeDeleted.getUserId());
			rowsAffected = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println(rowsAffected + " row(s) deleted.");
		
		return rowsAffected;
	}
	
	public static boolean update(User userToBeUpdated) {
		
		String sql = "UPDATE public.user SET username=? WHERE id = ?";
		
		PreparedStatement ps = null;
		
		int rowAffected = 0;
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, userToBeUpdated.getUsername());
			ps.setInt(2, userToBeUpdated.getUserId());
			
			rowAffected = ps.executeUpdate();
			System.out.println(rowAffected + " row(s) updated.");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rowAffected > 0;
	}
	

	public static Optional<User> checkExistByEmailPassowrd(String email, String password) {
		conn = DBConnection.getConnection();
		PreparedStatement ps = null;
		
		String sql = "select * from public.user where email=? AND password=?";

		User result = null;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, email);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				if (rs.getString("username") != null) {
					result = new Customer(rs.getInt("user_id"), rs.getString("username"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return Optional.ofNullable(result);
	}
	
	// For payment methods
	public static List<PaymentMethod> findPaymentMethod(int userId) {
		String sql = "select * from public.payment where customer_id=?";
		
		List<PaymentMethod> paymentMethods = new ArrayList<>();
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				PaymentMethod pm = null;
				pm = new PaymentMethod();				
				paymentMethods.add(pm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return paymentMethods;
	}
	
	public static int updatePaymentMethod(PaymentMethod payment) {
		String sql = "UPDATE public.payment_method SET " + 
					"credential=?" + 
					"WHERE id = ?";
		
		PreparedStatement ps = null;
		
		int rowAffected = 0;
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, payment.getCredential());
			ps.setInt(2, payment.getPaymentMethodId());
			
			rowAffected = ps.executeUpdate();
			System.out.println(rowAffected + " row(s) updated.");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rowAffected;
	}

	public static int insertPaymentMethod(Customer cust, PaymentMethod payment) {
		String sql = "INSERT INTO public.payment_method (customer_id, method_name, credential) "
				+ "VALUES (?,?,?)";
		
		PreparedStatement ps = null;
		
		int rowAffected = 0;
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, cust.getUserId());
			ps.setString(2, payment.getMethodName());
			ps.setString(3, payment.getCredential());
			
			rowAffected = ps.executeUpdate();
			System.out.println(rowAffected + " row(s) updated.");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();					
				}
				conn.commit();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rowAffected;
	}
	
	public static int deletePaymentMethod(PaymentMethod payment) {
		String sql = "DELETE FROM public.payment_method WHERE id = ?";
		
		PreparedStatement ps = null;
		
		int rowAffected = 0;
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, payment.getPaymentMethodId());
			
			rowAffected = ps.executeUpdate();
			System.out.println(rowAffected + " row(s) updated.");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();					
				}
				conn.commit();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rowAffected;
	}
	
	// For mailing address
	public static List<Address> findMailingAddress(int userId) {
		String sql = "select * from public.address where customerId=?";
		
		List<Address> addresses = new ArrayList<>();
		
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				Address addr = null;
				addr = new Address();				
				addresses.add(addr);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return addresses;
	}
	
	public static int updateMailingAddress(Address address) {
		String sql = "UPDATE public.address SET " + 
					"street=?,street2=?,postcode=?,city=?,state=?,country=?" + 
					"WHERE id = ?";
		
		PreparedStatement ps = null;
		
		int rowAffected = 0;
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, address.getStreet());
			ps.setString(2, address.getStreet2());
			ps.setString(3, address.getPostcode());
			ps.setString(4, address.getCity());
			ps.setString(5, address.getState());
			ps.setString(6, address.getCountry());
			ps.setInt(7, address.getId());
			
			rowAffected = ps.executeUpdate();
			System.out.println(rowAffected + " row(s) updated.");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				conn.commit();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rowAffected;
	}
	
	public static int insertMailingAddress(Customer cust, Address payment) {
		String sql = "INSERT INTO public.address (customer_id, street, street2, postcode, city, state, country) "
				+ "VALUES (?,?,?,?,?,?,?)";
		
		PreparedStatement ps = null;
		
		int rowAffected = 0;
		
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, cust.getUserId());
			ps.setString(2, payment.getStreet());
			ps.setString(3, payment.getStreet2());
			ps.setString(4, payment.getPostcode());
			ps.setString(5, payment.getCity());
			ps.setString(6, payment.getState());
			ps.setString(7, payment.getCountry());
			
			rowAffected = ps.executeUpdate();
			System.out.println(rowAffected + " row(s) updated.");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();					
				}
				conn.commit();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rowAffected;
	}

	final private static String getAuthenticationInfo = "SELECT * FROM public.authentication_info WHERE user_id=? AND authentication_type='password'";
	public static Optional<User> findUserWithAuthenticationInfo(String username) {
		conn = DBConnection.getConnection();
		PreparedStatement ps = null;

		User result = null;
		try {
			Optional<User> optUser = findByUsername(username);

			if (!optUser.isPresent()) {
				return Optional.empty();
			}
			result = optUser.get();
			ps = conn.prepareStatement(getAuthenticationInfo);
			ps.setInt(1, result.getUserId());
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				AuthenticationInfo authInfo = new AuthenticationInfo(rs.getString("authentication_type"), rs.getString("password"));
				result.setAuthenticationInfo(authInfo);
			}
			
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return Optional.ofNullable(result);
	}

}
