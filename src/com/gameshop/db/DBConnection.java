package com.gameshop.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * A singleton class which provides DB Connection
 */
public class DBConnection {

	private static Connection conn;

	private static String DB_HOST = "jdbc:postgresql://ec2-50-16-196-57.compute-1.amazonaws.com:5432/d1ojs7mle0kacl";
	private static String DB_USERNAME = "lwfdomkggzydlb";
	private static String DB_PASSWORD = "d4727048257f3aae2b2e7d1999c858a4a8ead8c9f176597cab04f5fc0286e55b";
	private static String DB_URL = "jdbc:postgresql://ec2-50-16-196-57.compute-1.amazonaws.com:5432/d1ojs7mle0kacl?user=lwfdomkggzydlb&password=d4727048257f3aae2b2e7d1999c858a4a8ead8c9f176597cab04f5fc0286e55b&sslmode=require";
	
	// db on the local machine
	private static String LOCAL_DB_HOST = "jdbc:postgresql:VaporGameShop";
	private static String LOCAL_DB_USERNAME = "postgres";
	private static String LOCAL_DB_PASSWORD = "idontknow";

	private static boolean connectToHerokuDB = false;
	
	// used by static factory method
	private DBConnection() {
		try {
			Class.forName("org.postgresql.Driver");
			System.err.println("Database driver found.");
			
			if (connectToHerokuDB) {
				String dbUrl = System.getenv("JDBC_DATABASE_URL");
				if (dbUrl == null) {
					// Not safe as the link may change periodically
					dbUrl = "jdbc:postgresql://ec2-50-16-196-57.compute-1.amazonaws.com:5432/d1ojs7mle0kacl?user=lwfdomkggzydlb&password=d4727048257f3aae2b2e7d1999c858a4a8ead8c9f176597cab04f5fc0286e55b&sslmode=require";
				}
				conn = DriverManager.getConnection(dbUrl);
			} else {
				conn = DriverManager.getConnection(LOCAL_DB_HOST, LOCAL_DB_USERNAME, LOCAL_DB_PASSWORD);
			}
			
//			conn.setAutoCommit(false);				
			System.out.println("Connected to the PostgreSQL server successfully.");			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("PostgreSQL driver not found.");
			e.printStackTrace();
		}
	}
	
	// Factory Method
	public static Connection getConnection() {
		if (conn != null) {
			return conn;
		} else {
			new DBConnection();
			return DBConnection.conn;
		}
	}
}
