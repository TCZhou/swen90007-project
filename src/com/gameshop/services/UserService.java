package com.gameshop.services;

import java.util.List;
import java.util.Optional;

import com.gameshop.mappers.UserMapper;
import com.gameshop.models.users.Address;
import com.gameshop.models.users.Customer;
import com.gameshop.models.users.PaymentMethod;
import com.gameshop.models.users.User;

public class UserService {

	// This method is used to register customer
	public static boolean registerCustomer(String email, String password) {
		final int INVALID_ID = -1;
		Customer cust = new Customer(INVALID_ID, null);
		int rowsAffected = UserMapper.insert(cust);
		
		if (rowsAffected == 1) {
			return true;
		} else {
			return false;
		}
	}

	// This method is used to verify if the customer is registered
	// or not
	public static boolean verifyUser(String email, String password) {
		
		Optional<User> user = UserMapper.checkExistByEmailPassowrd(email, password);
		
		if (user.isPresent()) {
			return true;
		}
		return false;
	}
	
	public static User verifyUserWithAuth(String username) {
		
		Optional<User> user = UserMapper.findUserWithAuthenticationInfo(username);
		
		if (user.isPresent()) {
			return user.get();
		}
		return null;
	}

	public static List<Address> findMailingAddresses(int userId) {
		List<Address> result = UserMapper.findMailingAddress(userId);
		return result;
	}

	public static List<PaymentMethod> findPaymentMethods(int userId) {
		List<PaymentMethod> result = UserMapper.findPaymentMethod(userId);
		return result;
	}
	
	public static User getUserWithAuthInfo(String username) {
		Optional<User> user = UserMapper.findUserWithAuthenticationInfo(username);
		return user.get();
	}
	
	public static User getUser(int userId) {
		Optional<User> user = UserMapper.find(userId);
		return user.get();
	}

}
