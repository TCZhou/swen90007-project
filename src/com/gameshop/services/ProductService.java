package com.gameshop.services;

import java.util.List;
import java.util.Optional;

import com.gameshop.mappers.ProductMapper;
import com.gameshop.models.Product;

public class ProductService {	

	// Method to get all products available
	public List<Product> getAllProducts() {
		List<Product> products = ProductMapper.findAllProducts();
		return products;
	}
	
	public Product getProductDetails(int productId) {
		Optional<Product> result = ProductMapper.find(productId);
		if (result.isPresent()) {
			Product p = result.get();
			return p;
		} else {
			return null;
		}
	}

	public boolean updateInventory(int productId, int inventory) {
		// TODO Auto-generated method stub
		Product productToUpdate = getProductDetails(productId);
		productToUpdate.setInventory(inventory);
		boolean success = ProductMapper.update(productToUpdate);
		return success;
	}
}
