package com.gameshop.models;

import java.io.Serializable;

/**
 * Product model
 */
public class Product implements Serializable {

	/**
	 * Serial Version ID
	 */
	private static final long serialVersionUID = 1L;

	private int productId;
	private String title;
	private double price;
	private String description;
	private String category;
	private String publisher;
	private int inventory;
	private int version;

	public Product(int i, String productName, double productPrice,
			String description, String category,String subCategory,
			String productManufacturer) {
		super();
		this.productId = i;
		this.title = productName;
		this.price = productPrice;
		this.description = description;
		this.category = category;
		this.publisher = productManufacturer;
	}

	public Product() {
	}

	public Product(int i, String productName, double productPrice,
			String description, String category, String productManufacturer) {
		super();
		this.productId = i;
		this.title = productName;
		this.price = productPrice;
		this.description = description;
		this.category = category;
		this.publisher = productManufacturer;
	}
	
	public Product(int id, String title, String publisher, double price) {
		super();
		this.productId = id;
		this.title = title;
		this.price = price;
		this.publisher = publisher;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return title;
	}

	public void setProductName(String productName) {
		this.title = productName;
	}

	public double getProductPrice() {
		return price;
	}

	public void setProductPrice(double productPrice) {
		this.price = productPrice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public int getInventory() {
		return inventory;
	}

	public void setInventory(int inventory) {
		this.inventory = inventory;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}
