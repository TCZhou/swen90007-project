package com.gameshop.models;

import java.util.List;

import com.gameshop.mappers.ProductMapper;

public class ProductCatalogue {
	private static ProductCatalogue instance = null;
	
	private List<Product> products;
	
	private ProductCatalogue() {
		// Retrieve a list of products from the database
		products = ProductMapper.findAllProducts();
	}
	
	public static ProductCatalogue getInstance() {
		if (instance == null) {
			instance = new ProductCatalogue();
		}
		return instance;
	}
	
	public List<Product> getAllProducts() {
		return products;
	}

}
