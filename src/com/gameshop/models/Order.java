package com.gameshop.models;

import java.util.List;

import com.gameshop.mappers.AddressMapper;
import com.gameshop.models.cart.ShoppingCartItem;
import com.gameshop.models.users.Address;
import com.gameshop.models.users.PaymentMethod;

public class Order {
	public static final int INVALID_ORDER_ID = -1;
	
	private int orderId;
	private int customerId;
	private String orderDate;
	private double total;
	private PaymentMethod paymentMethod;
	private Address deliverAddress;
	private List<ShoppingCartItem> items;

	// Id of the attribute for reference when need to look up
	private int paymentMethodId;
	private int addressId;
	
	public Order(int orderId, int customerId, String orderDate, double total, PaymentMethod paymentMethod,
			Address deliverAddress) {
		super();
		this.orderId = orderId;
		this.customerId = customerId;
		this.orderDate = orderDate;
		this.total = total;
		this.paymentMethod = paymentMethod;
		this.deliverAddress = deliverAddress;
	}
	
	public Order() {
		this.orderId = INVALID_ORDER_ID; 
	}

	public Order(int orderId, int customerId, String orderDate, double total, int paymentMethodId, int addressId) {
		// TODO Auto-generated constructor stub
		this.orderId = orderId;
		this.customerId = customerId;
		this.orderDate = orderDate;
		this.total = total;
		this.paymentMethodId = paymentMethodId;
		this.paymentMethod = null;
		this.addressId = addressId;
		this.deliverAddress = null;
	}
	
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public PaymentMethod getPaymentMethod() {
		if (paymentMethod == null && paymentMethodId > 0) {
			
		}
		return paymentMethod;
	}
	
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public void setPaymentMethodId(int paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public Address getDeliverAddress() {
		if (deliverAddress == null && addressId > 0) {
			deliverAddress = AddressMapper.findById(addressId);
		}
		return deliverAddress;
	}
	
	public void setDeliverAddress(Address deliverAddress) {
		this.deliverAddress = deliverAddress;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	
	public List<ShoppingCartItem> getItems() {
		return items;
	}

	public void setItems(List<ShoppingCartItem> items) {
		this.items = items;
	}
}
