package com.gameshop.models.users;

/**
 * User model
 */
public abstract class User {
	private int userId;
	private String username;
	private String email;
	private String address;
	private AuthenticationInfo authenticationInfo;
	
	public User(int userId, String username) {
		super();
		this.userId = userId;
		this.username = username;
	}
	
	public User(int userId, String username, String email, String address) {
		super();
		this.userId = userId;
		this.username = username;
		this.address = address;
		this.email = email;
	}
	
	public User(int userId, String username, AuthenticationInfo auth) {
		super();
		this.userId = userId;
		this.username = username;
		this.authenticationInfo = auth;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public AuthenticationInfo getAuthenticationInfo() {
		return authenticationInfo;
	}
	
	public void setAuthenticationInfo(AuthenticationInfo authenticationInfo) {
		this.authenticationInfo = authenticationInfo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
