package com.gameshop.models.users;

import java.util.List;

import com.gameshop.models.Order;
import com.gameshop.models.cart.ShoppingCart;
import com.gameshop.services.UserService;

public class Customer extends User {

	private List<Address> mailingAddresses = null;
	private List<PaymentMethod> paymentMethods = null;
	private ShoppingCart shoppingCart = null;
	private List<Order> orders = null;
	
	public Customer(int userId, String username) {
		super(userId, username);
		// TODO Auto-generated constructor stub
		mailingAddresses = null;
		paymentMethods = null;
		setOrders(null);
		setShoppingCart(null);
	}
	
	public Customer(int userId, String username, List<Address> addresses, List<PaymentMethod> paymentMethods) {
		super(userId, username);
		this.mailingAddresses = addresses;
		this.paymentMethods = paymentMethods;
	}

	public Customer(int id, String username, String email, String address) {
		super(id, username, email, address);
	}

	public List<Address> getMailingAddresses() {
		if (mailingAddresses == null) {
			loadMailingAddresses();
		}
		return mailingAddresses;
	}

	public void setMailingAddresses(List<Address> mailingAddresses) {
		this.mailingAddresses = mailingAddresses;
	}

	public List<PaymentMethod> getPaymentMethods() {
		if (paymentMethods == null) {
			loadPaymentMethods();
		}
		return paymentMethods;
	}
	
	public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public ShoppingCart getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(ShoppingCart shoppingCart) {
		this.shoppingCart = shoppingCart;
	}

	public List<Order> getOrders() {
		if (orders == null) {
			loadOrders();
		}
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	private void loadMailingAddresses() {
		mailingAddresses = UserService.findMailingAddresses(getUserId());
	}
	
	
	private void loadPaymentMethods() {
		paymentMethods = UserService.findPaymentMethods(getUserId());
	}
	
	private void loadOrders() {
		setOrders(null);
	}
	
}
