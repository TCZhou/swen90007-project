package com.gameshop.models.users;

import com.gameshop.models.ProductCatalogue;

public class Administrator extends User {
	
	private ProductCatalogue productCatalogue;
	
	public Administrator(int userId, String username) {
		super(userId, username);
		// TODO Auto-generated constructor stub
		setProductCatalogue(null);
	}
	
	public Administrator(int id, String username, String email, String address) {
		super(id, username, email, address);
	}

	public ProductCatalogue getProductCatalogue() {
		if (productCatalogue == null) {
			loadProductCatalogue();
		}
		return productCatalogue;
	}

	public void setProductCatalogue(ProductCatalogue productCatalogue) {
		this.productCatalogue = productCatalogue;
	}
	
	private void loadProductCatalogue() {
		setProductCatalogue(ProductCatalogue.getInstance());
	}
}
