package com.gameshop.models.users;

public class AuthenticationInfo {

	private String authenticationInfo;
	private String password;
	
	public AuthenticationInfo(String password) {
		this.authenticationInfo = "password";
		this.password = password;
	}
	
	public AuthenticationInfo(String authenticationinfo, String password) {
		this.authenticationInfo = authenticationinfo;
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String getAuthenticationInfo() {
		return authenticationInfo;
	}
}
