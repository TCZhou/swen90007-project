package com.gameshop.models.users;

public class PaymentMethod {
	private int paymentMethodId;
	private int userId;
	private String methodName;
	private String credential;
	
	public PaymentMethod() {
	}
	
	public PaymentMethod(int paymentMethodId, int userId, String methodName, String credentials) {
		super();
		this.paymentMethodId = paymentMethodId;
		this.userId = userId;
		this.methodName = methodName;
		this.credential = credentials;
	}

	public int getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(int paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public int getCustomerId() {
		return userId;
	}

	public void setCustomerId(int customerId) {
		this.userId = customerId;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getCredential() {
		return credential;
	}

	public void setCredential(String credential) {
		this.credential = credential;
	}
}
