package com.gameshop.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;

/**
 * Servlet implementation class LogoutController
 * This controller simply handles the logout request of the user
 */
@WebServlet(description = "The controller handling the logout request of the user", urlPatterns = { "/logout" })
public class LogoutController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogoutController() {
        super();
    }

	/** 
	 * Log out the user
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SecurityUtils.getSubject().logout();
		// Redirect the view to home page (or probably to a page saying you have successfully logged out)
		response.sendRedirect("./");
	}
}
