package com.gameshop.controllers;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;

import com.gameshop.dtos.ProductDTO;
import com.gameshop.models.Product;
import com.gameshop.remote_facade.RemoteFacade;
import com.gameshop.services.ProductService;

/**
 * Servlet implementation class RootServlet
 * Shows the list of all products in the store
 */
@WebServlet("/products")
public class ProductInfoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Product productInformation = null;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductInfoController() {
        super();
    }

    HttpSession hs;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int productId = Integer.parseInt(request.getParameter("id"));
		
		ProductService productService = new ProductService();
		productInformation = null;
		
		productInformation = productService.getProductDetails(productId);
		
		ProductDTO productInfoDto = new RemoteFacade().getProductDTO(productId);
		
		hs = request.getSession();
		hs.setAttribute("product", productInfoDto);
		hs.setAttribute("productID", productId);
		hs.setAttribute("alert", "");

		// Set Product Category and SubCategory in the Context Attribute
		ServletContext servletContext = getServletContext();
		
		servletContext.setAttribute("productCategory", productInformation.getCategory());
		String nextPage = "/WEB-INF/product.jsp";
			
		RequestDispatcher requestDispather = servletContext.getRequestDispatcher(nextPage);
		
		requestDispather.forward(request, response);
	}

	/**
	 * Update the inventory of the product
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!SecurityUtils.getSubject().hasRole("administrator")) {
			response.sendError(403);
			return;
		} else {
			// Allow the administrator to update the inventory
			int productId = Integer.parseInt(request.getParameter("productid"));
			int inventory = Integer.parseInt(request.getParameter("inventory"));
			boolean success = new ProductService().updateInventory(productId, inventory);
			
			ServletContext servletContext = getServletContext();
			hs = request.getSession();
			hs.setAttribute("productID", productId);
			
			if (success) {
				productInformation.setInventory(inventory);
				hs.setAttribute("product", productInformation);				
				hs.setAttribute("alert", "");
			} else {
				hs.setAttribute("alert", "Failed to update the inventory.");
			}
			
			// Redirect to product.jsp			
			String nextPage = "/WEB-INF/product.jsp";
			RequestDispatcher requestDispather = servletContext.getRequestDispatcher(nextPage);
			requestDispather.forward(request, response);
		}
	}
}
