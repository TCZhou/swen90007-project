package com.gameshop.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gameshop.models.Product;
import com.gameshop.models.cart.ShoppingCart;
import com.gameshop.services.ProductService;

/**
 * Servlet implementation class RootServlet
 * Shows the list of all products in the store
 */
@WebServlet("/")
public class RootController extends HttpServlet {
	private static final long serialVersionUID = 1L;
		
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RootController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String userPath = request.getServletPath();
		String getURL = "/" + userPath + ".jsp";
		
		ProductService productService = new ProductService();
		List<Product> productsList = null;
		productsList = productService.getAllProducts();
				
		ServletContext servletContext = getServletContext();
		
		servletContext.setAttribute("productsList", productsList);
		
		String nextPage = "/index.jsp";
			
		RequestDispatcher requestDispather = servletContext.getRequestDispatcher(nextPage);
		
		requestDispather.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
