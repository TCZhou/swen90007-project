package com.gameshop.controllers;

import java.io.IOException;
import java.security.Security;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.WebUtils;

/**
 * Servlet implementation class RootServlet Shows the list of all products in
 * the store
 */
@WebServlet("/login")
public class LoginController extends HttpServlet {

	/**
	 * Compulsory version ID required by serializable interface
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginController() {
		super();
	}

	/**
	 * Show the login page
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String page = "/WEB-INF/login.jsp";

		ServletContext servletContext = getServletContext();
		RequestDispatcher requestDispather = servletContext.getRequestDispatcher(page);

		requestDispather.forward(request, response);
	}

	/**
	 * Handles the login request from the user, authentication is done using Apache Shiro
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Extract the parameter of username and password
		Subject user = SecurityUtils.getSubject();
		boolean a = user.isAuthenticated();
		System.out.println("isAuthenticated: " + a);
		
		final String rememberOptionValue = request.getParameter("remember-option");
		boolean rememberMe = rememberOptionValue != null && rememberOptionValue.equals("remember-me");
		
		UsernamePasswordToken token = new UsernamePasswordToken(request.getParameter("username"), request.getParameter("password"));
		if (rememberMe) {
			token.setRememberMe(true);
		}
		
		// Attempt to login
		final String incorrectInfo = "Incorrect username / password. Please try again.";
		try {
		    user.login( token );
		    //if no exception, that's it, we're done!
		} catch ( UnknownAccountException uae ) {
		    //username wasn't in the system, show them an error message?
			goBackToLogin(request, response, incorrectInfo);
			return;
		} catch ( IncorrectCredentialsException ice ) {
		    //password didn't match, try again?
			goBackToLogin(request, response, incorrectInfo);
			return;
		} catch ( LockedAccountException lae ) {
		    //account for that username is locked - can't login.  Show them a message?
			goBackToLogin(request, response, lae.getMessage());
			return;
		} catch ( AuthenticationException ae ) {
		    //unexpected condition - error?
			goBackToLogin(request, response, ae.getMessage());
			return;
		}
		System.out.println("hasRole(adminstrator) = " + SecurityUtils.getSubject().hasRole("administrator"));
		// when succeeded, return to the previous page
		WebUtils.redirectToSavedRequest(request, response, "/");
	}
	
	private void goBackToLogin(HttpServletRequest request, HttpServletResponse response, String exceptionMsg) {
		String nextPage = "/WEB-INF/login.jsp";
		RequestDispatcher rd = getServletContext().getRequestDispatcher(nextPage);
		try {
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			e.printStackTrace();
		}
	}
}
