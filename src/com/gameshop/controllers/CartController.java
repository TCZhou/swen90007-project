package com.gameshop.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gameshop.models.Product;
import com.gameshop.models.cart.ShoppingCart;
import com.gameshop.services.ProductService;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("/cart/*")
public class CartController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private HttpSession hs;
//	private static final Logger LOGGER = LoggerFactory.getLogger(CartController.class.getName());

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CartController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Subject currentUser = SecurityUtils.getSubject();
		
		if (!currentUser.isAuthenticated()) {
			// Redirect to login page
			response.sendRedirect("login");
			return;
		}
		
		// Since the path always start by a '/', the element at index 0 would be an empty string
		// Get the last element in the array as the command

		String command = request.getPathInfo();
		
		if (command == null) {
			forwardToDefaultPage(request, response);
			return;
		} else {
			command = command.toLowerCase();			
		}
		
		hs = request.getSession();
		ShoppingCart cart = (ShoppingCart) hs.getAttribute("cart");
		
		// If user request to add products to shopping cart
		if (command.equals("/add")) {
			// Same as using the PUT method
			doPut(request, response);
		}
		// If user request to update the products
		else if (command.equals("/update")) {
			doPost(request, response);
		}
		// If user wants to remove an item from cart
		else if (command.equals("/remove")) {
			doDelete(request, response);
		} else {
			forwardToDefaultPage(request, response);
		}

	}

	private void forwardToDefaultPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nextPage = "/WEB-INF/cart.jsp";
		RequestDispatcher rd = getServletContext().getRequestDispatcher(nextPage);
		rd.forward(request, response);
	}

	/** 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String prod_id = request.getParameter("productid");
		int productid = Integer.parseInt(prod_id);
		int quantity = Integer.parseInt(request.getParameter("quantity"));

		Product product = (Product) new ProductService().getProductDetails(productid);

		ShoppingCart cart = (ShoppingCart) hs.getAttribute("cart");

		if (cart != null) {
			cart.updateQuantity(productid, quantity, product);
		}
		response.sendRedirect("cart.jsp");
	}

	/**
	 * Add an item into the cart
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		// Request the Session
		HttpSession hs = request.getSession();
		ShoppingCart cart = (ShoppingCart) hs.getAttribute("cart");

		Product product = (Product) hs.getAttribute("productToAdd");
		
		// Checks whether cart is available
		// If not, then create a cart object
		if (cart == null) {
//			LOGGER.debug("Creating cart to session");
			cart = new ShoppingCart();
			hs.setAttribute("cart", cart);
		}

		int prodID = Integer.parseInt((hs.getAttribute("productID").toString()));
		Integer productID = new Integer(prodID);
		// Check whether the product id is not null
		// If not null then add the product to the cart

		if (productID != null) {
			ProductService service = new ProductService();
			Product p = service.getProductDetails(productID);

			cart.add(productID, p);
			response.sendRedirect("product.jsp");

		}
	}
	
	/**
	 * Delete an item from the cart
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Request the Session
		hs = request.getSession();
		ShoppingCart cart = (ShoppingCart) hs.getAttribute("cart");
		
		int pid = Integer.parseInt(request.getParameter("productid"));

		if (cart != null) {
			cart.remove(pid);
			response.sendRedirect("cart.jsp");
		}
	}
}
