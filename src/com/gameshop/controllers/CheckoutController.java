package com.gameshop.controllers;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;

import com.gameshop.models.Product;
import com.gameshop.models.cart.ShoppingCart;
import com.gameshop.services.ProductService;

/**
 * Handle the checkout request of the user
 */
@WebServlet("/checkout")
public class CheckoutController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ShoppingCart shoppingCart = null;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckoutController() {
        super();
    }

    HttpSession hs;
    
	/**
	 * Show the snapshot of the order and let the user to make final decision
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		shoppingCart = null;
				
		hs = request.getSession();
		shoppingCart = (ShoppingCart) hs.getAttribute("shoppingCart");

		// Set Product Category and SubCategory in the Context Attribute
		ServletContext servletContext = getServletContext();
		
		String nextPage = "/WEB-INF/checkout.jsp";
			
		RequestDispatcher requestDispather = servletContext.getRequestDispatcher(nextPage);
		
		requestDispather.forward(request, response);
	}

	/**
	 * Update the inventory of the products
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		shoppingCart = null;
		
		hs = request.getSession();
		shoppingCart = (ShoppingCart) hs.getAttribute("shoppingCart");

		ServletContext servletContext = getServletContext();
		
		// Allow the administrator to update the inventory
		int productId = Integer.parseInt(request.getParameter("productid"));
		int inventory = Integer.parseInt(request.getParameter("inventory"));
		boolean success = new ProductService().updateInventory(productId, inventory);
		
		hs = request.getSession();
		hs.setAttribute("productID", productId);
		
		
		
		// Redirect to product.jsp
		String nextPage = "/WEB-INF/checkout.jsp";
		RequestDispatcher requestDispather = servletContext.getRequestDispatcher(nextPage);
		requestDispather.forward(request, response);
	}
}
