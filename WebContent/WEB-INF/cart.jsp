<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Shopping Cart - Vapor Game Shop</title>
<link href="${pageContext.request.contextPath}/css/shop-homepage.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

<link
	href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<style>
body {
	background-color: white;
}

.input-mini {
	diplay: inline;
}

#cartitem {
	display: inline;
}

#welcome {
	display: inline;
	height: 20px;
}

#cartitems.cart_table {
	border: 1px solid #E6E4DF;
	margin-bottom: 50px
}

#cart_items .cart_table .cart_menu {
	background: #000;
	color: #fff;
	font-size: 16px;
	font-family: 'Roboto', sans-serif;
	font-weight: normal;
}

#cart_items .cart_table .table.table-condensed thead tr {
	height: 51px;
}

#cart_items .cart_table .table.table-condensed tr {
	border-bottom: 1px solid #F7F7F0
}

#cart_items .cart_table .table.table-condensed tr:last-child {
	border-bottom: 0
}

.cart_table table tr td {
	border-top: 0 none;
	vertical-align: inherit;
}

#cart_items .cart_table .image {
	padding-left: 30px;
}

#cart_items .cart_table .cart_description h4 {
	padding-left: 50px;
	margin-bottom: 0
}

#cart_items .cart_table .cart_description h4 a {
	font-family: 'Roboto', sans-serif;
	font-size: 20px;
	font-weight: normal;
}

#cart_items .cart_table .cart_description p {
	padding-left: 50px;
	color: #696763
}

#cart_items .cart_table .cart_price p {
	color: #696763;
	font-size: 18px
}

#subtotal_cart {
	float: right;
	padding-left: 30px;
}

#cart_items .cart_table .cart_total_price {
	color: #FE980F;
	font-size: 24px;
}

#cart_items .cart_table .checkoutbutton {
	padding-right: 70px;
}

.cart_product {
	display: block;
	margin: 15px -70px 10px 25px;
}

.cart_quantity_button a {
	background: #F0F0E9;
	color: #696763;
	display: inline-block;
	font-size: 16px;
	height: 28px;
	overflow: hidden;
	text-align: center;
	width: 35px;
	float: left;
}

.input-mini {
	color: #696763;
	float: middle;
	font-size: 16px;
	text-align: center;
	font-family: 'Roboto', sans-serif;
}

.btn save_btn {
	vertical-align: inherit;
}

.cart_delete {
	display: block;
	margin-right: -12px;
	overflow: hidden;
}

.cart_delete a {
	background: #33C;
	color: #FFFFFF;
	padding: 7px 9px;
	font-size: 16px
}

.cart_delete a:hover {
	background: #000;
}
</style>
</head>
<body>	
	<!-- Navigation -->
	<%@include file = "/WEB-INF/jspf/navbar.jspf" %>
	
	<!-- Section -->
	<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ul class="breadcrumb">
				<li class="active"><a href="./">Home</a> <span
					class="divider">/</span> </li>
				<li class="active">Checkout</li>
			</ul>
		</div>
		<div class="table-responsive cart_table">
			<table id="product_table" class="table table-condensed">
				<thead>
					<tr class="cart_menu">
						<td class="image">Product</td>
						<td class="description">Description</td>
						<td class="quantity">Quantity</td>
						<td class="price">Price</td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="cart_products" items="${cart.getItems()}">
						<c:set var="products" value="${cart_products.getProduct()}" />
						<tr>
							<td class="cart_product">
								<a class="h5" href="product/?id=${ products.getProductId() }">
									<c:out value="${products.getProductName()}" />
								</a>
							</td>
							<td class="cart_description">
								<p class="info">
									Category:
									<c:out value="${products.getCategory()}" />
								</p>
								<p class="info">
									Publisher:
									<c:out value="${products.getPublisher()}" />
								</p>
							</td>
							<td class="cart_quantity">
								<div class="form-horizontal">
									<form action="cart/update" method="POST">
										<input type="hidden" name="productid"
											value="<c:out value="${products.productId}"/>" /><input
											type="number" name="quantity" size="2" maxlength="2"
											class="input-mini" max="10" min="1"
											value="<c:out value="${cart_products.quantity}"/>" />
										<button class="update btn btn-primary">Update</button>
									</form>
								</div>
							</td>
							<td class="cart_total">
								<p class="product_tot lead">
									<c:out value='${ String.format("%.2f", cart_products.getTotal())}' />
								</p>
							</td>
							<td class="cart_delete">
								<form action="cart/remove" method="GET">
									<input type="hidden" name="productid"
										value="<c:out value="${product.getProductId()}"/>" />
									<button type="submit"
										class="cart_quantity_delete btn btn-primary">
										<i class="fa fa-times" id="remove"></i>
									</button>
								</form>
							</td>
						</tr>
					</c:forEach>
					<tr>
						<td>&nbsp;</td>
						<td><a role="button" href="./checkout"
							id="checkoutbutton" class="btn btn-primary">Proceed to
								Checkout</a></td>
						<td><a id="continueshopping"
								class="btn btn-primary pull-left" href="./">Continue Shopping</a>
							<p id="subtotal_cart" class="lead">Total:</p></td>
						<td class="cart_sub_total">
							<p class="lead">
								<c:out value="${ String.format('%.2f', cart.getTotal() == null ? 0.0 : cart.getTotal()) }" />
							</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	</section>
	
	<script src="${pageContext.request.contextPath}/vendor/jquery/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/vendor/bootstrap/js/bootstrap.js"></script>
</body>
</html>