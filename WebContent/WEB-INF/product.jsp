<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Template referenced from: https://startbootstrap.com/template-overviews/shop-item/ -->
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product Information - Vapor Game Shop</title>
<link href="${pageContext.request.contextPath}/css/shop-homepage.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="${pageContext.request.contextPath}/vendor/bootstrap/css/bootstrap-responsive.css" rel="stylesheet" />

<style>
#cartitem {
	display: inline;
}

#welcome {
	display: inline;
	height: 20px;
}

.main-body {
	margin-bottom: 50px;
}
</style>
</head>
<body>
	<!-- Navigation -->
    <%@include file = "/WEB-INF/jspf/navbar.jspf" %>

    <!-- Page Content -->
    <div class="container main-body">
		<div class="row">

        <div class="col-lg-3">
          <h1 class="my-4">Product Information</h1>
          <div class="list-group">
            <a href="#" class="list-group-item active">Category 1</a>
            <a href="#" class="list-group-item">Category 2</a>
            <a href="#" class="list-group-item">Category 3</a>
          </div>
        </div>
        <!-- /.col-lg-3 -->
	<div class="col-lg-9">

          <div class="card mt-4">
            <img class="card-img-top img-fluid" src="http://placehold.it/900x400" alt="">
            <div class="card-body">
				
              <h3 class="card-title">${product.getProductName()}</h3>
			  <br/>            
              
              <h4>$${String.format("%.2f", product.getProductPrice())}</h4>
              <p class="card-text">Published by: ${product.getPublisher()}</p>
              <p class="card-text">Description:<br/>${product.getDescription()}</p>
              <p class="card-text">Inventory: ${product.inventory}</p>
              <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
              4.0 stars

              <br/>
              <br/>
              
	          <p class="card-text">
	          	<form action="./cart/add" method="get">
		          <input type="hidden" id="productId" value="${productID}" name="id"/>
				  <input class="btn btn-primary pull-right" id="btn-buynow" type="submit" value="Add to Cart" />
				</form>
	          </p>
	          
	          <shiro:hasRole name="administrator">
	          <c:if test="${not empty alert}">
	    		<div class="alert alert-warning">${alert}</div>
	    	  </c:if>
          	  <p>
          	  	<p>Update the number of inventory:</p>
	         	<form method="post" class="form-inline">
		          <input type="hidden" name="productid" value="${productID}" name="id"/>
		          <input class="form-control" id="inventory" type="number" min="0" name="inventory" value="${product.getInventory()}" />
				  <input class="btn btn-success pull-right" type="submit" value="Update Inventory" />
				</form>
	          </p>
	          </shiro:hasRole>
            </div>
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col-lg-9 -->

      </div>

    </div>
	
	<%@include file="/WEB-INF/jspf/footer.jspf" %>
	
	<script src="${pageContext.request.contextPath}/vendor/jquery/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/vendor/bootstrap/js/bootstrap.js"></script>
	
	<script>
		var productPrice = "${product.productPrice}";
		var productId = "${productID}";
		function validate() {
		   var inventory = document.getElementById("inventory").value; // get the value
		   var valid = inventory >= 0;                               // if length is > 0 then valid will be true, otherwise it will be false
		   document.getElementById("submit").disabled = !valid;  // disable if only valid is false
		}
		// Disables a button
		$(function disableButton() {
			jQuery.fn.extend({
				disable : function(state) {
					return this.each(function() {
						this.disabled = state;
					});
				}
			});

			$('#disabledbutton').disable(true);
		});
	</script>
	
</body>
</html>