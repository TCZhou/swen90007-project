<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- Referenced from Bootstrap example pages, link: https://getbootstrap.com/docs/4.1/examples/sign-in/ -->
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Log In</title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/css/login.css" rel="stylesheet">
  </head>

  <body class="text-center">
  	<!-- Navigation -->
    <%@include file = "/WEB-INF/jspf/navbar.jspf" %>
	
    <form class="form-signin" action="./login" method="post">
      <img class="mb-4" src="https://png.icons8.com/ultraviolet/80/000000/user-male-circle.png" alt="" width="100" height="100">
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
      <label for="inputEmail" class="sr-only">Username</label>
      <input type="text" id="inputEmail" class="form-control" name="username" placeholder="Username" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" name="remember-option" value="remember-me"> Remember me
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
      <p class="mt-5 mb-3 text-muted">Sample customer account: 
      	<br/> username: Adam <br/> password: hellow 
      	<br/> <br/>
      	<br/> Sample administrator account: 
      	<br/> username: admin <br/> password: administrator</p>
      <a class="mt-5 mb-3 text-muted href="https://icons8.com">Icon pack by Icons8</a>
    </form>
  </body>
</html>
