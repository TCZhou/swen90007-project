--CREATE DATABASE "VaporGameShop"
--  WITH ENCODING='UTF8'
--       CONNECTION LIMIT=-1;

CREATE TABLE public.user(
   id SERIAL,
   name VARCHAR(50) NOT NULL,
   email VARCHAR(100),
   address VARCHAR(200),
   PRIMARY KEY(id));

INSERT INTO public.user VALUES (001, 'jones', 'jones@gmail.com','Office 9, IAS, Princeton, USA');
INSERT INTO public.user VALUES (002, 'admin', 'shop.admin@vaporgameshop.com','Office 6, IAS, Princeton, USA');
INSERT INTO public.user VALUES (003, 'alex', 'alex@gmail.com', 'Office 9, IAS, Princeton, USA');
INSERT INTO public.user VALUES (004, 'adam', 'adam@gmail.com', '528 Swanston St, Carlton, Australia');
INSERT INTO public.user VALUES (005, 'david', 'david@gmail.com', 'SOME WHERE');
INSERT INTO public.user VALUES (006, 'admin2', 'admin@gameshop.com', '500 Collin St, Melbourne, Australia');


--DROP TABLE public.customer;
CREATE TABLE public.customer(
   user_id SERIAL REFERENCES public.user(id),
   PRIMARY KEY(user_id));

INSERT INTO public.customer VALUES (001);
INSERT INTO public.customer VALUES (003);
INSERT INTO public.customer VALUES (004);
INSERT INTO public.customer VALUES (005);

CREATE TABLE public.administrator(
   user_id SERIAL REFERENCES public.user(id),
   PRIMARY KEY(user_id));

INSERT INTO public.administrator VALUES (2);
INSERT INTO public.administrator VALUES (6);


--DROP TABLE public.category;
CREATE TABLE public.category(
	id SERIAL PRIMARY KEY,
	catalogue_name VARCHAR(50) NOT NULL
);

INSERT INTO public.category VALUES (DEFAULT, 'PC Games');
INSERT INTO public.category VALUES (DEFAULT, 'PS4 Games');
INSERT INTO public.category VALUES (DEFAULT, 'Nintendo Switch Games');
INSERT INTO public.category VALUES (DEFAULT, 'Xbox One Games');

--DROP TABLE public.address CASCADE;
CREATE TABLE public.address(
    id SERIAL,
    customer_id SERIAL REFERENCES public.customer(user_id),
    street VARCHAR(50) NOT NULL,
    street2 VARCHAR(50),
    postcode VARCHAR(10) NOT NULL,
    city VARCHAR(50) NOT NULL,
    "state" VARCHAR(50) NOT NULL,
    country VARCHAR(50) NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO public.address VALUES (DEFAULT, 001, 'Nowhere street', NULL, '123', 'Melbourne', 'VIC', 'Australia');
INSERT INTO public.address VALUES (DEFAULT, 001, 'Somewhere street', NULL, '321', 'Melbourne', 'VIC', 'Australia');
INSERT INTO public.address VALUES (DEFAULT, 003, 'Anywhere street', NULL, '312', 'Melbourne', 'VIC', 'Australia');

--DROP TABLE public.payment_method;
CREATE TABLE public.payment_method(
    id SERIAL,
    customer_id SERIAL REFERENCES public.customer(user_id),
    method_name VARCHAR(100) NOT NULL,
    "credential" VARCHAR(100) NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO public.payment_method VALUES (001, 001, 'Visa Credit', 'lakjsd;ijpoihqpoiwet^&*()');
INSERT INTO public.payment_method VALUES (002, 001, 'Paypal', 'lakjsd;#$%^hqpoiwet^&*(');
INSERT INTO public.payment_method VALUES (003, 003, 'MasterCard Debit', 'lakjsd;ijpoihqpoiwet^&*()');

--DROP TABLE public.product;

CREATE TABLE public.product(
    id SERIAL,
    product_name VARCHAR(100) NOT NULL,
    publisher VARCHAR(50),
    price REAL,
    "description" VARCHAR(400),
    category_id INT REFERENCES public.category(id),
    category VARCHAR(50),
    "image" VARCHAR(100),
    invenotry INT NOT NULL DEFAULT 0,
    ver INT NOT NULL DEFAULT 0
    PRIMARY KEY(id)
);

INSERT INTO public.product VALUES (DEFAULT, 'No mans sky', 'Sony Entertainment', 11.11, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla', 2, 'PS4 Games','https://picsum.photos/200/300/?random');
INSERT INTO public.product VALUES (DEFAULT, 'South Park: The Stick of Truth', 'Ubisoft', 22.22, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla',1, 'PC Games','https://picsum.photos/200/300/?random');
INSERT INTO public.product VALUES (DEFAULT, 'Mario Odessey', 'Nintendo', 33.33, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla',3, 'Nintendo Switch Games','https://picsum.photos/200/300/?random');
INSERT INTO public.product VALUES (DEFAULT, 'The legend of Zelda: Breath of the wind', 'Nintendo', 44.44, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla',3, 'Nintendo Switch Games','https://picsum.photos/200/300/?random');
INSERT INTO public.product VALUES (DEFAULT, 'Cup Head', 'StudioMDHR', 55.55, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla', 4, 'Xbox One Games','https://picsum.photos/200/300/?random');
INSERT INTO public.product VALUES (DEFAULT, 'Portal', 'Valve', 19.99, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla', 1, 'PC Games','https://picsum.photos/200/300/?random');
INSERT INTO public.product VALUES (DEFAULT, 'Spider-man', 'Sony Entertainment', 89.99, 'this is the description for spider man', 2, 'PS4 Games', 'https://picsum.photos/200/300/?random');
INSERT INTO public.product VALUES (DEFAULT, 'Uncharted', 'Sony Entertainment', 77.77,'this is the description for uncharted', 2, 'PS4 Games', 'https://picsum.photos/200/300/?random');
INSERT INTO public.product VALUES (DEFAULT, 'Destiny', 'Bungie', 65.56,'this is the description for destiny', 4, 'Xbox One Games', 'https://picsum.photos/200/300/?random');

--DROP TABLE public.order;
CREATE TABLE public.order(
    id SERIAL,
    customer_id SERIAL REFERENCES public.customer(user_id);
    order_date VARCHAR(20),
    total REAL,
    payment_method SERIAL REFERENCES public.payment_method(id),
    "address" SERIAL REFERENCES public.address(id),
    PRIMARY KEY(id)
);

--DROP TABLE public.authentication_info CASCADE;
CREATE TABLE public.authentication_info (
    user_id SERIAL REFERENCES public.user(id),
    authentication_type VARCHAR(20),
    password VARCHAR(50)
);

INSERT INTO public.authentication_info VALUES (1, 'password', 'randomString');
INSERT INTO public.authentication_info VALUES (2, 'password', 'administrator');
INSERT INTO public.authentication_info VALUES (3, 'password', 'kingOfNorthernLand');
INSERT INTO public.authentication_info VALUES (4, 'password', 'hellow');
INSERT INTO public.authentication_info VALUES (5, 'password', 'helloworld');
INSERT INTO public.authentication_info VALUES (6, 'password', 'administrator2');

CREATE TABLE public.order_item (
	order_id SERIAL REFERENCES public."order"(id) ON DELETE CASCADE ON UPDATE CASCADE,
	product_id SERIAL REFERENCES public.product(id) ON DELETE CASCADE ON UPDATE CASCADE,
	quantity int NOT NULL DEFAULT 1,
	PRIMARY KEY (order_id, product_id)
)
